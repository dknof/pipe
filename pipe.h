#pragma once

#include "pipe/add.h"
#include "pipe/combination.h"
#include "pipe/sort.h"
#include "pipe/sum.h"
#include "pipe/tee.h"
#include "pipe/unique.h"
#include "pipe/write.h"
