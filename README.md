# pipe

The pipe library adds to C++ a system for changing containers like the pipe system in the shell (cat text.txt | sort | unique).

It is simple to use and to expand.

I have created it as a proof of concept and testet it with a handful of filter commands. I regard it as finished.

## Motivation

One new feature in Java 8 are the [streams](http://www.oracle.com/technetwork/articles/java/ma14-java-se-8-streams-2177646.html). While this can easily be implemented in C++, I thought it a bad syntax:
* first the container must be converted in a stream
* each member can return a different type but the calls .().().() suggest the same object
* the analogous pipe system from the shell has a different syntax

Also I think that the Java-System is not expandable by a user, since he has to modify the stream class. This pipe system has each pipe command as a separate independend class. Further you can combine pipe commands together and use the combination later.

On the other side this pipe system does not support lazy evaluation and parallelism.

## Getting Started

* Get the sourcecode: `git clone https://gitlab.com/dknof/pipe.git`
* Call `make` to build the program
* start `./pipe-test` to show the test results

## Examples

pipe is the namespace. sort, unique and write are filter commands which can be used to create a modified vector.

For further examples: Just look at main.cpp

### simple pipe
```
std::vector<std::string>{"A", "C", "B", "A", "C", "A", "B"} | pipe::sort() | pipe::unique() | pipe::write(std::cout);
```
→ A, B, C

### saved command combination
```
auto sort_unique_write = pipe::combine(pipe::sort(), pipe::unique(), pipe::write(cout));
std::vector<std::string>{"A", "C", "B", "A", "C", "A", "B"} | sort_unique_write;
```
→ A, B, C


## Contributing

Pipe is an open source project. Feel free to contact the [author](mailto:dknof+pipe@posteo.de).

## License

GNU GENERAL PUBLIC LICENSE Version 3

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

## See also

See [https://github.com/jscheiny/Streams](https://github.com/jscheiny/Streams) for a more advanced library with lazy evaluation.

## Project status

This project is under no active developement (status of Octobre 10, 2019).
I regard it to bee finished as proof of concept.
