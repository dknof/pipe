#!/bin/zsh

#export CXX="ccache clang++"
export CXXFLAGS="-Wall -Werror -Wno-parentheses -std=c++14 -O0 -ggdb"
#export CXXFLAGS="-Wall -Werror -Wno-parentheses -std=c++14 -O3"
#export CXXFLAGS="-Wall -Werror -Wno-parentheses -std=c++14 -O0 -ggdb -ferror-limit=2"

make || exit

echo "starting pipe-test…"
./pipe-test "$@"
