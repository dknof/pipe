#include "pipe.h"
#include <iostream>
using std::cout;
#include <string>
#include <vector>

int main()
{
  {
    cout << "\n## String tests\n";
    std::vector<std::string> v = {"A", "C", "xxx", "U", "C", "A"};
    {
      cout << "\nTest 1 (write): A, C, xxx, U, C, A\n";
      v | pipe::write(cout);
    }
    {
      cout << "\nTest 2 (sort): A, A, C, C, U, xxx\n";
      v | pipe::sort() | pipe::write(cout);
    }
    {
      cout << "\nTest 3 (unique): A, C, U, xxx\n";
      v | pipe::sort() | pipe::unique() | pipe::write(cout);
    }
    {
      cout << "\nTest 4 (tee): 2 * A, C, U, xxx\n";
      auto w = decltype(v){};
      v | pipe::sort() | pipe::unique() | pipe::tee(w) | pipe::write(cout);
      w | pipe::write(cout);
    }
    {
      cout << "\nTest 5 (combine): A, C, U, xxx\n";
      auto sort_unique_write = pipe::combine(pipe::sort(), pipe::unique(), pipe::write(cout));
      v | sort_unique_write;
    }
  }

  {
    cout << "\n## Number tests\n";
    std::vector<int> v = { 1, 2, 3, 4, 5};
    {
      cout << "\nTest 1 (add): 3, 4, 5, 6, 7\n";
      v | pipe::add(2) | pipe::write(cout);
    }
    {
      cout << "\nTest 2 (sum): 15\n";
      cout << (v | pipe::sum()) << '\n';
    }
  }

  return 0;
} // int main()
