PROGRAM = pipe-test
CXX ?= g++
CXXFLAGS ?= -std=c++14 -O0 -Wall -Werror
DEPGEN_FLAGS ?= -MMD -MP -MT $@ -MF $(@:.o=.d)
LDFLAGS ?=
SOURCES = main.cpp

.PHONY: all
all : $(PROGRAM)

$(PROGRAM): main.o
	$(CXX) $(CXXFLAGS) $(LDFLAGS) $^ -o $@

%.o : %.cpp
	$(CXX) $(CXXFLAGS) $(DEPGEN_FLAGS) -c $< -o $@

clean:
	rm -f *.o "$(PROGRAM)"

-include $(SOURCES:%.cpp=%.d)
