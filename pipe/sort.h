#pragma once

#include <algorithm>

namespace pipe {
/** sort the container
 **/
class sort {
  public:
    sort() = default;

    template<typename T>
      T pipe(T lhs)
      {
        std::sort(lhs.begin(), lhs.end());
        return lhs;
      }
};
} // namespace pipe

template<typename T>
T
operator|(T lhs, pipe::sort rhs)
{
  return rhs.pipe(lhs);
}
