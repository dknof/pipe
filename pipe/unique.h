#pragma once

#include <algorithm>

namespace pipe {
/** remove duplicates
 **/
class unique {
  public:
    unique() = default;

    template<typename T>
      T pipe(T lhs)
      {
        auto p = std::unique(lhs.begin(), lhs.end());
        lhs.erase(p, lhs.end());
        return lhs;
      }
};
} // namespace pipe

template<typename T>
T
operator|(T lhs, pipe::unique rhs)
{
  return rhs.pipe(lhs);
}
