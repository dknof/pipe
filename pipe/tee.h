#pragma once

#include <type_traits>

namespace pipe {
/** write a copy of the vector in the given argument
 ** construct with function tee(x)
 **/
template<typename T>
  class tee_p {
    public:
      tee_p() = delete;
      tee_p(T& copy) :
        copy_(copy)
    { }

      T pipe(T lhs)
      {
        copy_ = lhs;
        return lhs;
      }
    private:
      T& copy_;
  };

template<typename T>
  tee_p<T>
  tee(T& copy)
  {
    return tee_p<T>(copy);
  }
} // namespace pipe

template<typename T>
T
operator|(T lhs, pipe::tee_p<T> rhs)
{
  return rhs.pipe(lhs);
}
