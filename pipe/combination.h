#pragma once

namespace pipe {
/** combine two pipe commands
 **/
template<typename T1, typename T2>
class combination {
  public:
    combination() = delete;
      combination(T1 first, T2 second) :
        first_(first), second_(second)
  { }

      template<typename T>
        auto pipe(T lhs)
        {
          return (lhs | first_ | second_);
        }
  private:
      T1 first_;
      T2 second_;
};

template<typename T1, typename T2>
auto
combine(T1 t1, T2 t2)
{
  return combination<T1, T2>(t1, t2);
}
template<typename T1, typename T2, typename... Args>
auto
combine(T1 t1, T2 t2, Args... args)
{
  return combine(combine(t1, t2), args...);
}
} // namespace pipe

template<typename T, typename T1, typename T2>
auto
operator|(T lhs, pipe::combination<T1, T2> rhs)
{
  return rhs.pipe(lhs);
}
