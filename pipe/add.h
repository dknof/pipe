#pragma once

namespace pipe {
/** add a value
 **/
template<typename T>
class add_p {
  public:
    add_p() = delete;
      add_p(T summand) :
        summand_(summand)
  { }

      template<typename T2>
        T2 pipe(T2 lhs)
        {
          for (auto& x : lhs)
            x += summand_;
          return lhs;
        }
  private:
      T const summand_;
};

template<typename T>
add_p<T>
add(T summand)
{
  return add_p<T>(summand);
}
} // namespace pipe

template<typename T, typename T2>
T2
operator|(T2 lhs, pipe::add_p<T> rhs)
{
  return rhs.pipe(lhs);
}
