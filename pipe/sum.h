#pragma once

#include <algorithm>
#include <numeric>

namespace pipe {
/** sum a value
 **/
class sum {
  public:
    sum() = default;

    template<typename T>
      auto pipe(T lhs)
      {
        return std::accumulate(lhs.begin(), lhs.end(), 0);
      }
};

} // namespace pipe

template<typename T>
auto
operator|(T lhs, pipe::sum rhs)
{
  return rhs.pipe(lhs);
}
