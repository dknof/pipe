#pragma once

#include <iostream>
#include <string>

namespace pipe {
/** Write the container into the given output stream
 **/
class write {
  public:
    write() = delete;
    explicit write(std::ostream& ostr,
                   std::string const separator = ", ",
                   std::string const last = "\n") :
      ostr_(&ostr), separator_(separator), last_(last)
  { }

    template<typename T>
      void pipe(T lhs)
      {
        auto i = lhs.begin();
        *ostr_ << *i;
        for (++i; i != lhs.end(); ++i) 
          *ostr_ << separator_ << *i;
        *ostr_ << last_;
      }

  private:
    std::ostream* ostr_;
    std::string const separator_;
    std::string const last_;
};
} // namespace pipe

template<typename T>
void
operator|(T lhs, pipe::write rhs)
{
  return rhs.pipe(lhs);
}
